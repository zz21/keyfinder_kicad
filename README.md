# keyfinder_KiCad

## 1.Nouveau Capteur

**Nom du capteur :** IM69D130
**Type :** Capteur de son
**Fabriquant :** Infineon Technologies
**Référence :** https://www.mouser.fr/ProductDetail/Infineon-Technologies/IM69D130V01XTSA1?qs=W0yvOO0ixfHjr98Yrg6FIA%3D%3D

## 2.Utilité pour le keyfinder

**Fonctionnalités :** 

1. *Détection audio :* Le microphone est capable de détecter les signaux audio dans l'environnement immédiat du keyfinder.

2. *Activation par le son :* Le keyfinder peut être configuré pour s'activer en réponse à des bruits spécifiques, tels que des sifflements ou des sons spécifiques définis par l'utilisateur.

3. *Localisation auditive :* Le keyfinder peut être utilisé pour localiser la source d'un son, aidant ainsi à retrouver les objets égarés.

4. *Alertes sonores personnalisées :* Les utilisateurs ont la possibilité de définir des alertes sonores personnalisées, permettant au keyfinder d'émettre des signaux sonores distincts en fonction de certaines conditions détectées.

**Avantage pour le produit :** 

1. *Localisation plus précise :* Le microphone permet une localisation plus précise des objets perdus en utilisant des indices sonores. Cela offre une alternative ou un complément efficace à la localisation visuelle.

2. *Activation intelligente :* La capacité d'activation basée sur des signaux sonores spécifiques rend le keyfinder plus intelligent. Il ne réagira qu'aux sons préconfigurés, réduisant les fausses alertes.

3. *Personnalisation des alertes :* Les utilisateurs peuvent personnaliser les alertes sonores en fonction de leurs préférences, offrant une expérience plus individualisée.

4. *Réduction des risques de perte :* La combinaison de la localisation visuelle et auditive réduit les risques de perte d'objets en offrant une méthode de recherche plus robuste.

5. *Technologie avancée :* L'intégration d'un microphone démontre l'adoption de technologies avancées, renforçant la position du keyfinder en tant que produit moderne et innovant.

## 3.Démarche de recherche

**Technologie :** La technologie utilisé par le capteur IM69D130 est la technologie MEMS.Mon choix s'est tourné vers ce type de technologie grâce aux nombreux avantages qu'offrent cette techologie, tels que la petite taille et la consommation d'énergie réduite. 

***Taille :** Comme dit précédement, la taille du capteur est très important à prendre en compte pour pouvoir l'intégrer sur le PCB. J'ai donc cherché un capteur de petite dimension et le IM69D130 correspond à mes attentes. Ses dimensions sont de 4mmX3mmX1.2mm

**Prix :** Le prix lui aussi joue un rôle important dans le choix du capteur. Sur le site *Mouser Electronics*, l'achat de 1000 capteurs revient à 0.977€ l'unité.  

**Disponibilité :** Il est indiqué sur le site que le stock est de presque 16 000 et expédiable immédiatement.   

**Autre critères de sélection :** Sensibilité élevée (-36 dB), capacité à capturer un large spectre de fréquences audio (2.9 MHz à 3.3MHz), la sortie numérique (Digital) simplifie l'intégration électronique avec le reste du système du keyfinder, faible consommation d'énergie.

# 4.Intégration dans le keyfinder

Je n'ai eu aucun problème à intégrer le capteur, que ce soit au niveau électrique ou au niveau mécanique. 
Cependant, j'ai rencontré un problème avec le support de pile dans la partie mécanique de KiCad. En effet, j'arrive à placer le support dans le PCB, puisqu'il se situe sur la deuxième couche. Cependant, une fois qu'il est positionné, je n'arrive pas à effectuer le routage entre des éléments qui sont situés au même endroit que le support mais sur l'autre couche. J'ai donc décidé de ne pas intégrer la pile pour le moment, mais cela pourrait poser problème si nous souhaitions créer le PCB.






